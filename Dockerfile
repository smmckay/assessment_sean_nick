FROM php:7.1

COPY . /usr/src/myapp
CMD [ "php", "/index.php"]
